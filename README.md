# F-strings
[![pipeline status](https://gitlab.com/Miridius/f-strings/badges/master/pipeline.svg)](https://gitlab.com/Miridius/f-strings/commits/master)

A simple Clojure implementation of the fantastic Python 3.6 feature,
[f-strings](https://docs.python.org/3/reference/lexical_analysis.html#f-strings).

F-strings are a new way to merge data and strings in a way that reduces noise,
is easy to read and use, and contains maximum power in the minimum space.

I have tried to adjust and extend the python f-strings implementation to make
it more Clojuresque and to take advantage of some of Clojure's features such
as keyword lookups.

This library has no dependencies, and exposes only a single macro: `f`

## Getting Started

Include the following in your
[deps.edn](https://www.clojure.org/guides/deps_and_cli) file:
```clojure
{:deps {f-strings {:git/url "https://gitlab.com/Miridius/f-strings"
                   :sha "3803cfdbe8f3beade4926ea9f8fed22fe31b5883"}}}
```

Then, simply import the f macro into your code and you're ready to go!

```clojure
(require '[f-strings.core :refer [f]])

(let [greeting-data {:greeting "Hello" :name "World"}]
  (f ":greeting, (apply str (shuffle (seq :name)))!"
     greeting-data))
;; => "Hello, Wlrod!"
```

## Usage

### Standard f-strings (Python style, but with parens)

Similar to python, an f-string is a string literal which contains forms that
should be evaluated and their results placed within the output string. These
forms are evaluated the same scope as the call to f. In Python, the forms
are surrounded with {}, but, since this is Clojure, we will use parens ():

```clojure
(f "1 + 1 is (+ 1 1)!")
;; => "1 + 1 is 2!"
```

Note: the parens are included as part of the form which is evaluated, since
most of the time that's what we want. But there can be cases where it's
desirable to just pass a static value and not execute a function, in which
case one workaround is to wrap that value in a call to `(str)`:
```clojure
(def x 100)
(f "x is (str x).")
;; => "x is 100."
```

### Keyword lookups (not in Python!)

When f is passed a map as a second argument, all keywords in the f-string are
substituted for map lookups:

```clojure
(f "x = :x" {:x 1})
;; => "x = 1"
```

Vectors of keywords can also be passed to look up nested values:

```clojure
(f "[:a :b]" {:a {:b 1} :b {:a 2}})
;; => "1"
```

Sometimes the macro may be overeager in determining what is part of your
keyword and what is not (e.g. it might include some trailing punctuation).
You can avoid this by simply wrapping your keyword in a vector:
```clojure
(f "x is :x!" {:x 10})    ;; doesn't work because it treats :x! as the keyword
;; => "x is "
(f "x is [:x]!" {:x 10})  ;; works
;; => "x is 10!"
```

All features can be mixed and matched as needed, for example keyword or
vectors of keywords can be placed inside a paren form:
```clojure
(def triangle {:b 10 :h 5 :style {:color "red"}})
(f "Area = (* 1/2 :b :h), Color = [:style :color]" triangle)
;; => "Area = 25, Color = red"
```

### Missing features / known issues

1. There is no support yet for a format minispec, but it is planned for the
   future.
2. Currently you must pass a string literal to the f macro, it's not possible
   to pass a variable containing a string because macros cannot evaluate their
   parameters. If you have an idea for how to fix this, let me know!

## Development

### Prerequisites

1. Java 1.6+ (recommended: Java 1.8+)
2. Install the [Clojure CLI tools](https://www.clojure.org/guides/deps_and_cli)
3. `git clone https://gitlab.com/Miridius/f-strings && cd f-strings`

### Start a REPL with `clj`

```clojure
$ clj
Clojure 1.9.0
user=> (use 'f-strings.core)
nil
user=> (f "(inc (inc 0))")
"2"
```

You can use `macroexpand-1` and `macroexpand` to test the workings of the macro
```clojure
user=> (def data {:a 1 :b {:c 2}})
#'user/data
user=> (macroexpand-1 '(f "a = :a, c + 1 = (inc [:b :c])" data))
(f "a = (:a data), c + 1 = (inc (get-in data [:b :c]))")
user=> (macroexpand '(f "a = :a, c + 1 = (inc [:b :c])" data))
(str "a = " (:a data) ", c + 1 = " (inc (get-in data [:b :c])))
user=> (f "a = :a, c + 1 = (inc [:b :c])" data)
"a = 1, c + 1 = 3"
```

### Run the tests with `clj -A:test`

```bash
$ clj -A:test

Running tests in #{"test"}

Testing f-strings.core-test

Ran 3 tests containing 13 assertions.
0 failures, 0 errors.
```

### Update the pom.xml (for Cursive users) with `clj -Spom`

[Cursive](https://cursive-ide.com/) does not yet support deps.edn, but the
following command will create a pom.xml file for Cursive to use. Run this
any time you modify the dependencies in deps.edn:
```bash
$ clj -Spom
```

For more information see the [Cursive issue tracker](https://github.com/cursive-ide/cursive/issues/1910).


## License

Copyright © 2018 David Rolle

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.