(ns f-strings.core-test
  (:require [clojure.test :refer :all]
            [f-strings.core :refer :all]))

;; since tokenize-f-str is private we need to look up the var directly
(def tokenize-f-str #'f-strings.core/tokenize-f-str)

(deftest test-tokenize-f-str
  (testing "nested parens"
    (is (= ["abc" '(1 (2)) "def"] (tokenize-f-str "abc(1(2))def"))))

  (testing "escaped parens"
    (is (= ["abc(1" '(2) ")def"] (tokenize-f-str "abc^(1(2)^)def"))))

  (testing "no nils"
    (is (empty? (tokenize-f-str "")))
    (is (= ['(1)] (tokenize-f-str "(1)")))))


(defmacro f?
  "Test the result of parsing an f-string against the output"
  ([f-str output]
   `(is (= ~output (f ~f-str))))
  ([f-str data-map output]
   `(is (= ~output (f ~f-str ~data-map)))))

(deftest test-f-str
  (testing "plain text only"
    (f? "hello" "hello")
    (f? "" ""))

  (testing "expanding single forms"
    (f? "(+ 1 1)" "2")
    (f? "(/ 1 3)(reduce + [1 2 3 4])" "1/310"))

  (testing "forms with nested parens"
    (f? "(reduce + (range (inc 4)))" "10"))

  (testing "local state & escaped parens"
    (let [point {:x 100 :y 200}]
      (f? "point^(x^): (:x point), point^(y^): (:y point)"
          "point(x): 100, point(y): 200"))))

(deftest test-f-with-map
  (testing "single keywords"
    (let [point {:x 100 :y 200}]
      (is (= "x = 100, y = 200" (f "x = :x, y = :y" point)))))

  (testing "keywords and keyword vectors"
    (let [point {:x 100 :y {:z 200}}]
      (f? "x = :x, z = [:y :z]" point
          "x = 100, z = 200"))))

